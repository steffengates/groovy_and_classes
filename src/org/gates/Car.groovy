package org.gates

class Car{
    String make
    String model
    int year

    // Overriding toString like this will set the behavior when you "println" an
    // instance of this class
    String toString(){
        "${year} ${make} ${model}"
    }
}
