package org.gates
import org.gates.util.*
import org.gates.*

// This is a groovy script instead of a formal groovy class.
// It has different variable scope rules, and doesn't require the
// more formal, java-style syntax of class declaration. It's a bit like
// ruby or shell scripting.

println "Hello, world!"

println "I'm going to make use of the MathUtils now"
println "5 - 3 = ${MathUtils.sub(5, 3)}"

println "I can also create instances of objects, just like in a " +
    "tradtional application:"

Person me = new Person(firstName:"Steffen", lastName:"Gates")
println me

println "And I can even make an instance of our 'App' class, and " +
    "use its methods."
App app = new App()
println(app.greeting(me.firstName))
