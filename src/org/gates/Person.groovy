package org.gates

class Person{
    String firstName
    String lastName
    def garage = []

    // Overriding toString like this will set the behavior when you "println" an
    // instance of this class
    String toString(){
        "$lastName, $firstName"
    }
}
