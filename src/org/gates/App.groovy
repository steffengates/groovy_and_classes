package org.gates

import org.gates.util.MathUtils

class App{
    static void main(args){
        println "hello, world!"
        println "I'm going to add two numbers: 2 + 3"
        println "MathUtils has static methods (class methods) which we " +
            "can use without creating an instance of MathUtils."
        println "The sum is: ${MathUtils.add(2,3)}"

        println "-----------------"

        println "I'm going to create a number of cars."
        println "Because the \"Car\" class is in the same package as " +
            "this App class, we don't need to import it like we did " +
            "with MathUtils."

        Car car1 = new Car(make:'Scion', model:'tc', year:2013)
        println car1
        Car car2 = new Car(make:'Honda', model:'Pilot', year:2015)
        println car2
        Car car3 = new Car(make:'Toyota', model:'Prius', year:2017)
        println car3

        println "Because these are my cars, I'll add them to my garage."

        Person me = new Person(firstName:"Steffen", lastName:"Gates")
        me.garage = [car1, car2, car3]

        println "My garge has these cars in it: ${me.garage}"
    }

    def greeting(String name){
        "Hello, $name"
    }
}
