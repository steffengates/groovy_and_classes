groovy_and_classes
=============================================

Structure
---------------------------------------------

    src/                <-- All the source for our application
        |
        org             <-- java convention is to make packages in the
            |           <com/org>.<name>.<whatever>.<you>.<want> format
            gates       <-- my package
                |
                util    <-- the package where we'll put our utility classes
            App.groovy  <-- If we're making a traditional app with a main
                            method (like c/java), then we might do it like this
            Car.groovy      <-- A pojo (plain old groovy object)
            Person.groovy   <-- Same as above
            Script.groovy   <-- A groovy "script". No main method, no compiling
                                required for this.

App vs Script
---------------------------------------------

An app is a more formal structure with rules similar to java. You have to have
a main method as an entry point, it must be compiled into a class file before
running it, and you create fields and methods.

A script is more like ruby/shell where you have global variable scoping, no
formal class structure requirements, and you can execute the script with the
groovy interpreter directly.

Packages
---------------------------------------------

Packages are just folders. You don't have to have them, but it's a good idea for
organization. All the rules for java packages apply here.

Compiling the App
---------------------------------------------

Go to the source directory, and use groovyc:

    $ cd groovy_and_classes/src
    $ groovyc org/gates/App.groovy

If you need to make changes to the compiled code, you need to remove the old
class files first.

    $ rm org/gates/*.class

Running the App
---------------------------------------------

Go to the source directory and run the App.class compiled file. Omit the .class
extension. Make sure you compiled first.

    $ cd groovy_and_classes/src
    $ groovy org/gates/App

Running the Script
---------------------------------------------

The script itself does not need to be compiled, but it uses some of the other
classes in the package. We need to compile those before we can use them in a
script. Do that (see above), and then:

    $ cd groovy_and_classes/src
    $ groovy org/gates/Script.groovy

Next Steps
---------------------------------------------

No one actually manages compilation and artifact management themselves. There
are several tools to do this, but the most popular and modern is
[gradle](https://gradle.org). Gradle expects a certain project structure, and
will use a build.gradle file to orchestrate dependencies, build process,
building distributions, packaging, etc...
